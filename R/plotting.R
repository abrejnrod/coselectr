#' Cophylo plot of a specific gene pair
#'
#' @param pair
#'
#' @return
#' @export
#'
#' @examples
#'
cophylo_plot_pair = function(pair, filename = "cophylo.pdf",  height = 15, width = 15){
  gene1 = sub("-", "", pair[[1]]$Genes[1])
  gene2 = sub("-", "", pair[[2]]$Genes[1])

  pair1aln = alnfilelistf[gene1][[1]]
  pair1dist = alnfilelistfdist[[gene1]]
  pair1dup = duplicated(pair1dist)

  pair2aln = alnfilelistf[gene2][[1]]
  pair2dist = alnfilelistfdist[[gene2]]
  pair2dup = duplicated(pair2dist)
  pair2distsub = pair2dist[!pair2dup, !pair2dup]

  #Arrange labels for the cophylo plot
  pair1distsubtree = midpoint.root(quick_tree(pair1aln))
  pair2distsubtree = midpoint.root((quick_tree(pair2aln)))
  pair1distsubtreec = chronos(pair1distsubtree)
  pair2distsubtreec = chronos(pair2distsubtree)
  pair1distsubtreesub = drop.tip(pair1distsubtreec, sample(pair1distsubtree$tip.label, 70))
  pair2distsubtreesub = drop.tip(pair2distsubtreec, sample(pair2distsubtree$tip.label, 70))

  pair1distsubtreelabels = data.frame(LabelDisp = paste0(pair[[1]]$Genes[1],"_", 1:length(pair1distsubtreesub$tip.label)),  Label = pair1distsubtreesub$tip.label, Contig = sapply(pair1distsubtreesub$tip.label, function(x) paste(strsplit(x, "_")[[1]][1:6], collapse = "_") ))
  pair2distsubtreelabels = data.frame(LabelDisp = paste0(pair[[2]]$Genes[1],"_", 1:length(pair2distsubtreesub$tip.label)), Label = pair2distsubtreesub$tip.label, Contig = sapply(pair2distsubtreesub$tip.label, function(x) paste(strsplit(x, "_")[[1]][1:6], collapse = "_") ))
  pair1distsubtreesub$tip.label = paste0(pair[[1]]$Genes[1],"_", 1:length(pair1distsubtreesub$tip.label))
  pair2distsubtreesub$tip.label = paste0(pair[[2]]$Genes[1],"_", 1:length(pair2distsubtreesub$tip.label))

  pairmerge = merge(pair1distsubtreelabels, pair2distsubtreelabels, by = "Contig"); dim(pairmerge)
  obj = cophylo(pair1distsubtreesub,pair2distsubtreesub, assoc = pairmerge[,c("LabelDisp.x", "LabelDisp.y")], rotate = F)
  pdf(file = filename, height = height, width = width)
  plot(obj, lwd = 3)
  dev.off()
}
