# CoselectR

CoselectR is an R package that generates hypotheses about protein-protein co-evolution. It does this by applying host-parasite correlation models to infer if proteins found in the same genetic context seems to be correlated at the amino acid level.
The pipeline supports a variety of methods for running the computationally intense parts of the pipeline to distribute it to compute clusters.

# Installation

```
devtools::install_bitbucket("https://bitbucket.org/abrejnrod/coselectr/")
library(coselectR)
```

# Usage example
